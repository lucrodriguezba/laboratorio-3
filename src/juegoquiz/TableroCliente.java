/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegoquiz;

/**
 *
 * @author Francely
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class TableroCliente extends JPanel implements ActionListener, KeyListener {

    private Timer timer;
    private ArrayList<Circulo> circulo;
    private Carro personajePrincipal;
    private CarroSecundario personajeSecundario;
    private int puntaje1 = 0;
    private int puntaje2 = 0;

    public TableroCliente() {
        this.setFocusable(true);
        this.addKeyListener(this);
        this.personajePrincipal = new Carro(100,200, Color.blue);
        this.personajeSecundario = new CarroSecundario(100,200,Color.RED);
        this.circulo = new ArrayList<Circulo>();
        this.circulo.add(new Circulo(20, 60));
        this.circulo.add(new Circulo(100, 80));
        this.circulo.add(new Circulo(180, 120));
        this.timer = new Timer(50, this);
        this.timer.start();
        Thread moveCharacter = new Thread(new HandlePos(this.personajeSecundario));
        moveCharacter.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Circulo c : this.circulo)
            c.dibujar(g, this);

        this.personajePrincipal.dibujar(g, this);
        this.personajeSecundario.dibujar(g, this);
        g.setColor(Color.blue);
        g.drawString("Puntaje carro azul: " + puntaje1, 40, 20);
        g.setColor(Color.red);
        g.drawString("Puntaje carro rojo: " + puntaje2, 40, 40);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        validarColisiones();
        for (Circulo c : this.circulo) 
            c.mover();

        Thread coins = new Thread(new moveCoins(circulo));
        coins.start();
        repaint();
    }

    public void validarColisiones() {
        Rectangle recPersonaje1 = this.personajePrincipal.obtenerRectangulo();
        Rectangle recPersonaje2 = this.personajeSecundario.obtenerRectangulo();
        ArrayList<Circulo> copia = (ArrayList<Circulo>) this.circulo.clone();
        for (Circulo c : circulo) {
            Rectangle RecCir = c.obtenerRectangulo();
            if (recPersonaje1.intersects(RecCir)){
                copia.remove(c);
                this.puntaje1++;
            }
            if (recPersonaje2.intersects(RecCir)){
                copia.remove(c);
                this.puntaje2++;
            }
        }
        this.circulo = copia;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        try {
            this.personajePrincipal.keyPressed(e);
        } catch (IOException ex) {
            Logger.getLogger(TableroCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(new Dimension(250, 250));
        frame.add(new TableroCliente());//adicionando el panel
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
}

class moveCoins implements Runnable{
    ArrayList<Circulo> circulo;

    public moveCoins(ArrayList<Circulo> circulo) {
        this.circulo=circulo;
    }
    
    @Override
    public void run() {
        for(int i=0;i<circulo.size();i++){
            circulo.get(i).mover();
        }
    }
    
}

class HandlePos implements Runnable {

    private Socket jugador;
    private int direccionx;
    private int direcciony;
    private Carro personaje;

    public HandlePos(Carro personaje){
        try {
            this.jugador = new Socket("localhost",8000);
        } catch (IOException ex) {
            Logger.getLogger(HandlePos.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.personaje = personaje;
    }

    @Override
    public void run() {
        try {
            DataInputStream fromServerX = new DataInputStream(jugador.getInputStream());
            DataInputStream fromServerY = new DataInputStream(jugador.getInputStream());
            while (true) {
                direccionx = fromServerX.readInt();
                direcciony = fromServerY.readInt();
                this.personaje.setX(direccionx);
                this.personaje.setY(direcciony);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public int getDireccionx() {
        return direccionx;
    }

    public int getDirecciony() {
        return direcciony;
    }

}
