/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegoquiz;

/**
 *
 * @author Francely
 */
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class CarroSecundario extends Carro{
    
    public CarroSecundario(int x, int y, Color color){
        this.x = x;
        this.y = y;
        this.color = color;
    }
    
    @Override
    public void keyPressed(KeyEvent e) throws IOException {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }        
    }
    
}

