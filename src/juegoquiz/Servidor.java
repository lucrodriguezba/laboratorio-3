/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegoquiz;

/**
 *
 * @author Francely
 */
import java.awt.BorderLayout;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Servidor extends JFrame {

    int direcciony;
    int direccionx;
    ServerSocket serverSocket;

    public Servidor() throws IOException {
        JTextArea jtaLog = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(jtaLog);
        add(scrollPane, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 300);
        setTitle("Servidor");
        setVisible(true);
        this.serverSocket = new ServerSocket(8000);
        jtaLog.append("Server started at " + new Date() + '\n');
        while (true){
            Socket jugador1 = serverSocket.accept();
            jtaLog.append("El primer jugador se ha unido");
            Socket jugador2 = serverSocket.accept();
            jtaLog.append("El segundo jugador se ha unido");
            DataInputStream inputFromJugador1X = new DataInputStream(jugador1.getInputStream());
            DataInputStream inputFromJugador1Y = new DataInputStream(jugador1.getInputStream());
            DataOutputStream outputToJugador2X = new DataOutputStream(jugador2.getOutputStream());
            DataOutputStream outputToJugador2Y = new DataOutputStream(jugador2.getOutputStream());
            direccionx = inputFromJugador1X.readInt();
            direcciony = inputFromJugador1Y.readInt();
            System.out.println(direccionx);
            System.out.println(direcciony);
            outputToJugador2X.writeInt(direccionx);
            outputToJugador2Y.writeInt(direcciony);
        }
    }
    
     public static void main(String[] args) {
        try {
            Servidor frame = new Servidor();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
}

