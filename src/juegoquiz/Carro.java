/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegoquiz;

/**
 *
 * @author Francely
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JPanel;

public class Carro {

    protected int x;
    protected int y;
    protected Color color;
    private DataOutputStream toServerX;
    private DataOutputStream toServerY;

    public Carro(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Carro() {
        this.x = 20;
        this.y = 20;
    }

    public void dibujar(Graphics g, JPanel panel) {
        g.setColor(Color.BLACK);
        g.fillOval(x + 10, y - 10, 10, 10);
        g.fillOval(x + 30, y - 10, 10, 10);
        g.setColor(color);
        int[] xPoints = {x, x, x + 10, x + 20, x + 30, x + 40, x + 50, x + 50};
        int[] yPoints = {y - 10, y - 20, y - 20, y - 30, y - 30, y - 20, y - 20, y - 10};
        g.fillPolygon(xPoints, yPoints, 8);
    }

    public void keyPressed(KeyEvent e) throws IOException {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }        
        Server(x, y);
    }

    public void mover() {
        this.x += 1;
        //this.y+=1;
    }

    public Rectangle obtenerRectangulo() {
        return new Rectangle(this.x, this.y - 30, 50, 30);
    }

    private void Server(int x, int y) throws IOException {
        try {
            // Create a socket to connect to the server
            Socket jugador = new Socket("localhost", 8000);
            // Create an output stream to send data to the server
            toServerX = new DataOutputStream(jugador.getOutputStream());
            toServerY = new DataOutputStream(jugador.getOutputStream());
            toServerX.writeInt(x);
            toServerX.flush();
            toServerY.writeInt(y);
            toServerY.flush();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
